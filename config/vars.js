if(process.env.NODE_ENV === 'production') {
  let vars;
  try{
    vars = require('./vars_prod.js');
  }catch(err) {
    throw new Error("Please ensure config/vars_prod.js file exists and has all keys");
  }
  module.exports = vars;
}else{
  let vars;

  try{
    vars = require('./vars_dev.js');
  }catch(err) {
    throw new Error("Please set up config/example.vars_dev.js file");
  }
  module.exports = vars;
}