/* 
  This file is used as a template for storing your variables like DB URI
  or JWT SECRET
  Rename it to vars-dev.js
  and add any keys you may need
*/

module.exports = {
  mongodbUri: ""
}