const mongoose = require('mongoose');
let {mongodbUri} = require('../config/vars');

console.log(mongodbUri);

module.exports = () => {
  return mongoose
    .connect(mongodbUri, {useNewUrlParser: true})
    .then(() => {
      return mongoose;
      console.log('connected to mongoDB');
    }).catch(err => {
      console.log(err);
    });
}
