const Worker = require('../models/workers.model');
const connectToDb = require('../connect');

const mongoose = require('mongoose');
let {mongodbUri} = require('../../config/vars');

connectToDb()
  .then((mongoose) => {
    seedWorkers(mongoose);
  });

function seedWorkers(mongoose) {
  console.log("SEED");
  
  const workers = [
    new Worker({
      firstName: 'John',
      lastName: 'Doe',
      position: 'DevOps',
      birthDate: '1992-05-01',
      salary: 2000
    }),
    new Worker({
      firstName: 'Jane',
      lastName: 'Doe',
      position: 'UI/UX Designer',
      birthDate: '1994-05-06',
      salary: 1700
    }),
    new Worker({
      firstName: 'Sam',
      lastName: 'Mitchel',
      position: 'Front End Dev',
      birthDate: '1992-10-09',
      salary: 2500
    })
  ];
  
  for (let i = 0; i < workers.length; i++) {
    workers[i].save((err, result) => {
      if(err) {
        throw new Error('Error while seeding workers', err);
      }
      if(i === workers.length - 1) {
        console.log('Successfully added data');
        mongoose.disconnect();
      }
    });
  }
}