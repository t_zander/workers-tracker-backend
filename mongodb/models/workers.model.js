const mongoose = require('mongoose');

const workersSchema = mongoose.Schema({
  firstName: {type: String, required: true},
  lastName: {type: String, required: true},
  position: {type: String, required: true},
  birthDate: {type: String, required: true},
  salary: {type: Number, required: true}
},
{
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
})

workersSchema.virtual('fullName').get(function() {
  return `${this.firstName} ${this.lastName}`;
})

module.exports = mongoose.model('Worker', workersSchema);
