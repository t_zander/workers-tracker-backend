# Project Title

Workers tracker backend

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing
```
Rename config/example.vars_dev.js to vars_dev.js
```
(Add any vars to this file if needed)

```
npm install
```
```
npm run dev -- to run in dev mode
```
## Deployment

Check config/vars_prod and add all variables to it

## Built With

* [Express.js](http://expressjs.com/) - The web framework used

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details