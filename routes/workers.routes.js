const router = require('express').Router();
const workersController = require('../controllers/workers.controller');
const { check, validationResult } = require('express-validator');


router.get('/', workersController.getAllWorkers);
router.post('/', workersController.addWorker);
router.patch('/:workerId', workersController.updateWorker);
router.delete('/:workerId', workersController.deleteWorkerById);

module.exports = router;