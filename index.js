const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const connectToMongoDB = require('./mongodb/connect');

const PORT = 8000 || process.env.PORT;

/* ROUTES IMPORT */
const workersRoutes = require('./routes/workers.routes');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());
app.use(express.static('public'));

/* ROUTES */
app.use('/api/workers', workersRoutes);

connectToMongoDB();
app.listen(PORT, () => {
  console.log('Successfully connected to server', PORT);
});