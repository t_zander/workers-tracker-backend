const Worker = require('../mongodb/models/workers.model');

module.exports = {
  getAllWorkers(req, res) {
    Worker
      .find()
      .then((workers) => {
        console.log(workers);
        return res.status(200).json({workers});
      })
  },
  addWorker(req, res) {
    const worker = new Worker({...req.body.worker});

    worker.save()
      .then((worker) => {
        res.status(201).json({worker});
      })
      .catch((error) => {
        res.status(500).json({error});
      })
  },
  updateWorker(req, res) {
    const workerId = req.params.workerId;
    console.log(req.body);

    Worker.findByIdAndUpdate(workerId, req.body.worker)
      .then(() => {
        res.status(200).json({message: 'Worker info successfully updated!'})
      })
  },
  deleteWorkerById(req, res) {
    const workerId = req.params.workerId;
    Worker.findByIdAndDelete(workerId)
      .then((resp) => {
        res.status(200).json({message: 'Successfully deleted!'})
      })
  }
}
